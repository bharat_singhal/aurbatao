class AddAttachmentAvatarClipToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts do |t|
      t.attachment :avatar
      t.attachment :clip
    end
  end

  def self.down
    remove_attachment :posts, :avatar
    remove_attachment :posts, :clip
  end
end
