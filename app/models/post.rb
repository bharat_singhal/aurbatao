class Post < ActiveRecord::Base
	acts_as_ordered_taggable
    # Attachment code for image
	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

    # attachement code for audio 
     #has_attached_file :clip
      
     #Here we have removed the validation for the clip to be an optional attachment
     #validates_attachment_presence :clip
     #validates_attachment_content_type :clip, :content_type => [ 'audio/mpg','application/mp3', 'application/x-mp3', 'audio/mpeg', 'audio/mp3' ],
                                #    :message => 'file must be of filetype .mp3'

     #validates_attachment_size :clip, :less_than => 10.megabytes 


     

	def self.get_tag_list
		tag_objects = ActsAsTaggableOn::Tag.all
		tag_list = []
		tag_objects.each do |tag_object|
			tag_list << tag_object.name
		end 
        return tag_list
	end
  def per_page
    2
      
  end


end
