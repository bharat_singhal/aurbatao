class WelcomeController < ApplicationController
	def index
		if params[:tag]
			@posts = Post.tagged_with(params[:tag]).paginate(page: params[:page], :per_page => 2)
		else
			@posts = Post.all.order('created_at DESC').paginate(page: params[:page], :per_page => 2)
		end 

		respond_to do |format|
			format.html
			puts("After the respond_to")
			#format.json{render json: @posts}
			format.js
		end 
		
	end
end
