class PostsController < ApplicationController
	autocomplete :post, :tagcolumn

	def new
		
	end

	def create
		@post = Post.new post_params
		# tagcolumn is a redundant column just to have a autocomplete feature working
		@post.tagcolumn = @post.tag_list

		if @post.save!
			redirect_to root_path
		else
		    render 'welcome/index' 
		end 
	end

	def edit
		
	end


	private

	def post_params
		params.require(:post).permit(:message, :tag_list, :avatar)
	end
end
